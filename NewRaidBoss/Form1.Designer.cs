﻿namespace RaidBoss
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.dayOfTheWeek = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bossName = new System.Windows.Forms.Label();
            this.battleField = new System.Windows.Forms.Label();
            this.timeZone = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dayOfTheWeek
            // 
            this.dayOfTheWeek.FormattingEnabled = true;
            this.dayOfTheWeek.ItemHeight = 12;
            this.dayOfTheWeek.Location = new System.Drawing.Point(48, 78);
            this.dayOfTheWeek.Name = "dayOfTheWeek";
            this.dayOfTheWeek.Size = new System.Drawing.Size(120, 88);
            this.dayOfTheWeek.TabIndex = 0;
            this.dayOfTheWeek.SelectedIndexChanged += new System.EventHandler(this.DOTWSelected);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(223, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "ボス";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("メイリオ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(223, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 30);
            this.label2.TabIndex = 2;
            this.label2.Text = "出現場所";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("メイリオ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(223, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 30);
            this.label3.TabIndex = 3;
            this.label3.Text = "出現時間";
            // 
            // bossName
            // 
            this.bossName.AutoSize = true;
            this.bossName.Font = new System.Drawing.Font("メイリオ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.bossName.Location = new System.Drawing.Point(352, 44);
            this.bossName.Name = "bossName";
            this.bossName.Size = new System.Drawing.Size(46, 30);
            this.bossName.TabIndex = 4;
            this.bossName.Text = "???";
            // 
            // battleField
            // 
            this.battleField.AutoSize = true;
            this.battleField.Font = new System.Drawing.Font("メイリオ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.battleField.Location = new System.Drawing.Point(352, 122);
            this.battleField.Name = "battleField";
            this.battleField.Size = new System.Drawing.Size(46, 30);
            this.battleField.TabIndex = 5;
            this.battleField.Text = "???";
            // 
            // timeZone
            // 
            this.timeZone.AutoSize = true;
            this.timeZone.Font = new System.Drawing.Font("メイリオ", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.timeZone.Location = new System.Drawing.Point(352, 212);
            this.timeZone.Name = "timeZone";
            this.timeZone.Size = new System.Drawing.Size(46, 30);
            this.timeZone.TabIndex = 6;
            this.timeZone.Text = "???";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(579, 289);
            this.Controls.Add(this.timeZone);
            this.Controls.Add(this.battleField);
            this.Controls.Add(this.bossName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dayOfTheWeek);
            this.Name = "Form1";
            this.Text = "レイドボス情報";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox dayOfTheWeek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label bossName;
        private System.Windows.Forms.Label battleField;
        private System.Windows.Forms.Label timeZone;
    }
}

