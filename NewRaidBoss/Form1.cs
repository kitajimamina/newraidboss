﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RaidBoss
{
    public partial class Form1 : Form
    {
        //ボスデータリスト
        List<Data> datas = new List<Data>();

        public Form1()
        {
            InitializeComponent();
            this.datas = new List<Data>();

            //ファイルからデータを読み込む
            ReadFromFile();

            //リストに曜日を表示する
            foreach (Data d in datas)
            {
                this.dayOfTheWeek.Items.Add(d.Dtow);
            }
        }

        private void ReadFromFile()
        {
            using (System.IO.StreamReader file =
                new System.IO.StreamReader(@"..\..\bossData.txt"))
            {
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();
                    string[] data = line.Split(',');

                    int id = int.Parse(data[0]);
                    string name = data[1];
                    string place = data[2];
                    string time = data[3];
                    string dtow = data[4];

                    datas.Add(new Data(id, name, place, time, dtow));
                }
            }
        }

        //情報リセット
        private void ResetDataInfo()
        {
            //名前、場所、時間、をリセット
            bossName.Text = "???";
            battleField.Text = "???";
            timeZone.Text = "???";
        }

        //レイドボスの情報を表示
        private void ShowDataInfo(int index)
        {
            //各ラベルに値表示
            bossName.Text = datas[index].Name;
            battleField.Text = datas[index].Place;
            timeZone.Text = datas[index].Time;
        }

        private void DOTWSelected(object sender, EventArgs e)
        {
            //情報リセット
            ResetDataInfo();

            //選択した曜日に対応する情報を表示する
            int index = this.dayOfTheWeek.SelectedIndex;
            ShowDataInfo(index);
        }
    }
}