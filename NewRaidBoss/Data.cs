﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidBoss
{
    class Data
    {
        private int id;
        private string name;
        private string place;
        private string time;
        private string dtow;

        //コンストラクタ自動生成
        public Data(int id, string name, string place, string time, string dtow)
        {
            this.Id = id;
            this.Name = name;
            this.Place = place;
            this.Time = time;
            this.Dtow = dtow;
        }

        //ゲッター・セッター自動生成
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Place { get => place; set => place = value; }
        public string Time { get => time; set => time = value; }
        public string Dtow { get => dtow; set => dtow = value; }
    }
}